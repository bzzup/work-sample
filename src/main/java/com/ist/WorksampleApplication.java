package com.ist;

import com.ist.model.*;
import com.ist.service.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashSet;

@SpringBootApplication
public class WorksampleApplication {

    private static final Logger log = LoggerFactory.getLogger(WorksampleApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WorksampleApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(SubjectServiceImpl subjectService, GradeLevelServiceImpl gradeLevelService,
                                               FormServiceImpl formService, StudentServiceImpl studentService, SubjectTypeServiceImpl subjectTypeService,
                                               GroupTemplatesServiceImpl groupTemplatesService) {
        return (String... args) -> {
            subjectTypeService.create(new SubjectType("Mandatory"));
            subjectTypeService.create(new SubjectType("Optional"));

            subjectService.createSubject(new Subject(subjectTypeService.getByName("Mandatory"), "BIO1", "Biology"));
            subjectService.createSubject(new Subject(subjectTypeService.getByName("Optional"), "MAT1", "Mathematics"));
            subjectService.createSubject(new Subject(subjectTypeService.getByName("Mandatory"), "ENG1", "English"));
            subjectService.createSubject(new Subject(subjectTypeService.getByName("Optional"), "RUS1", "Russian"));
            subjectService.createSubject(new Subject(subjectTypeService.getByName("Mandatory"), "PSY1", "Psychology"));

            gradeLevelService.createGradeLevel(new GradeLevel("1"));
            gradeLevelService.createGradeLevel(new GradeLevel("2"));
            gradeLevelService.createGradeLevel(new GradeLevel("3"));

            GroupTemplate groupTemplateEng = new GroupTemplate("ENG", 5, 1);
            groupTemplateEng.addSubject(subjectService.getSubjectByCode("ENG1").get(0));
            groupTemplateEng.addGradeLevel(gradeLevelService.getGradeLevelByCode("1"));
            groupTemplatesService.create(groupTemplateEng);

            GroupTemplate groupTemplatePsy = new GroupTemplate("PSY", 5, 1);
            groupTemplatePsy.addSubject(subjectService.getSubjectByCode("PSY1").get(0));
            groupTemplatePsy.addSubject(subjectService.getSubjectByCode("RUS1").get(0));
            groupTemplatePsy.addGradeLevel(gradeLevelService.getGradeLevelByCode("2"));
            groupTemplatesService.create(groupTemplatePsy);

            Form form1 = new Form("form1", gradeLevelService.getGradeLevelByCode("1"));
            Form form2 = new Form("form2", new HashSet<>(gradeLevelService.getAllGradeLevels()));
            formService.createForm(form1);
            formService.createForm(form2);

            log.info("=====CREATE STUDENTS=====");
            for (int i = 1; i < 11; i++) {
                Student student = new Student("Student "+i, gradeLevelService.getGradeLevelByCode("1"));
                studentService.createStudent(student);
                studentService.addStudentToForm(student, formService.getFormsByName("form1").get(0));
                for (Subject subject : subjectService.getAllSubjects()) {
                    studentService.addSubject(student, subject);
                }
            }

            for (int i = 1; i < 11; i++) {
                Student student = new Student("Student "+i, gradeLevelService.getGradeLevelByCode("2"));
                studentService.createStudent(student);
                studentService.addStudentToForm(student, formService.getFormsByName("form2").get(0));
                for (Subject subject : subjectService.getAllSubjects()) {
                    studentService.addSubject(student, subject);
                }
            }

//            Map<Student, List<Subject>> unplaced = groupTemplatesService.generate();

        };
    }
}
