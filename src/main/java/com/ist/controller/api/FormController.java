package com.ist.controller.api;

import com.ist.model.Form;
import com.ist.service.impl.FormServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

/**
 * Created by andhr on 2017-03-17.
 */

@RestController
@RequestMapping(value = "/forms", produces = APPLICATION_STREAM_JSON_VALUE)
public class FormController {

    @Autowired
    FormServiceImpl formService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Form> getAllForms() {
        return formService.getAllForms();
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public Form createForm(@RequestBody @Valid Form form) {
        return formService.createForm(form);
    }
}
