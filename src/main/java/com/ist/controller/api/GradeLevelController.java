package com.ist.controller.api;

import com.ist.model.GradeLevel;
import com.ist.service.impl.GradeLevelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

/**
 * Created by andhr on 2017-03-17.
 */
@RestController
@RequestMapping(value = "/gradelevels", produces = APPLICATION_STREAM_JSON_VALUE)
public class GradeLevelController {

    @Autowired
    GradeLevelServiceImpl gradeLevelService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<GradeLevel> getAllGradeLevels() {
        return gradeLevelService.getAllGradeLevels();
    }

    @RequestMapping(value = "/gradelevel", method = RequestMethod.POST)
    public GradeLevel createGradeLevel(@RequestBody @Valid GradeLevel gradeLevel) {
        return gradeLevelService.createGradeLevel(gradeLevel);
    }
}
