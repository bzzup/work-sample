package com.ist.controller.api;

import com.ist.model.Group;
import com.ist.model.Student;
import com.ist.model.Subject;
import com.ist.service.impl.GroupServiceImpl;
import com.ist.service.impl.GroupTemplatesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

/**
 * Created by andhr on 2017-03-19.
 */
@RestController
@RequestMapping(value = "/groups", produces = APPLICATION_STREAM_JSON_VALUE)
public class GroupController {
    @Autowired
    GroupServiceImpl groupService;

    @Autowired
    GroupTemplatesServiceImpl groupTemplatesService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Group> getAllGroups() {
        return groupService.getAll();
    }

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public Map<Student, List<Subject>> generate() {
        return groupTemplatesService.generate();
    }
}
