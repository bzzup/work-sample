package com.ist.controller.api;

import com.ist.model.Form;
import com.ist.model.GradeLevel;
import com.ist.model.Student;
import com.ist.service.impl.FormServiceImpl;
import com.ist.service.impl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

/**
 * Created by andhr on 2017-03-19.
 */
@RestController
@RequestMapping(value = "/students", produces = APPLICATION_STREAM_JSON_VALUE)
public class StudentController {
    @Autowired
    StudentServiceImpl studentService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Set<Student> getAllStudents() {
        return studentService.getAll();
    }

    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public Student createStudent(@RequestBody @Valid Student student) {
        return studentService.createStudent(student);
    }

    @RequestMapping(value = "/form/{form}", method = RequestMethod.GET)
    public Set<Student> getStudentsByForm(@PathVariable("form") Long formId) {
        return studentService.getStudentsByFormId(formId);
    }

    @RequestMapping(value = "/gradelevel/{gradelevel}", method = RequestMethod.GET)
    public Set<Student> getStudentsByGradeLevel(@PathVariable("gradelevel") Long gradelevelId) {
        return studentService.getStudentsByGradeLevelId(gradelevelId);
    }
}
