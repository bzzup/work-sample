package com.ist.controller.api;

import com.ist.model.Subject;
import com.ist.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

/**
 * Created by andhr on 2017-03-17.
 */
@RestController
@RequestMapping(value = "/subjects", produces = APPLICATION_STREAM_JSON_VALUE)
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Subject> getAllSubjects() {
        return subjectService.getAllSubjects();
    }

    @RequestMapping(value = "/subject", method = RequestMethod.POST)
    public Subject createSubject(@RequestBody @Valid Subject subject) {
        return subjectService.createSubject(subject);
    }
}
