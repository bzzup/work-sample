package com.ist.exception;

/**
 * Created by andhr on 2017-03-18.
 */
public class StudentException extends RuntimeException{

    public StudentException(String message) {
        super(message);
    }

    public StudentException(String message, Throwable cause) {
        super(message, cause);
    }

    public StudentException(Throwable cause) {
        super(cause);
    }
}
