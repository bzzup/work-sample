package com.ist.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */

@Entity
@Table(name = "form")
public class Form implements Serializable {

    private Long id;
    private String name;
    private Set<GradeLevel> gradeLevels = new HashSet<>(0);
    private Set<Student> students = new HashSet<>(0);

    public Form() {}

    public Form(String name) {
        this.name = name;
    }

    public Form(String name, Set<GradeLevel> gradeLevels) {
        this.name = name;
        this.gradeLevels = gradeLevels;
    }

    public Form(String name, GradeLevel gradeLevel) {
        this.name = name;
        this.gradeLevels.add(gradeLevel);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "form_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "form_gradelevel", joinColumns = {
            @JoinColumn(name = "form_id") },
            inverseJoinColumns = { @JoinColumn(name = "gradelevel_id") })
    public Set<GradeLevel> getGradeLevels() {
        return gradeLevels;
    }

    public void setGradeLevels(Set<GradeLevel> gradeLevels) {
        this.gradeLevels = gradeLevels;
    }

    @ManyToMany(mappedBy = "forms", fetch = FetchType.EAGER)
    @JsonIgnore
    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return String.format("Form [id = %d, Name = '%s', GL = '%s']", id, name, gradeLevels);
    }
}
