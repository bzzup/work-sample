package com.ist.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by andhr on 2017-03-17.
 */

@Entity
@Table(name = "gradelevel")
public class GradeLevel implements Serializable {

    private Long id;
    private String code;

    public GradeLevel() {}

    public GradeLevel(String code) {
        this.code = code;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gradelevel_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GradeLevel that = (GradeLevel) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(code, that.code)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(13, 35)
                .append(id)
                .append(code)
                .toHashCode();
    }

    @Override
    public String toString() {
        return code;
    }
}
