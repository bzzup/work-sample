package com.ist.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */
@Entity
@Table(name = "education_group")
public class Group implements Serializable {

    private Long id;
    private String groupName;
    private Set<Student> students = new HashSet<>(0);
    private Set<Subject> subjects = new HashSet<>(0);
    private Set<GradeLevel> gradeLevels = new HashSet<>(0);

    public Group(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "group_student", joinColumns = {
            @JoinColumn(name = "group_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "student_id",
                    nullable = false, updatable = false) })
    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "group_subject", joinColumns = {
            @JoinColumn(name = "group_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "subject_id",
                    nullable = false, updatable = false) })
    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    public void addGradeLevel(GradeLevel gradeLevel) {
        this.gradeLevels.add(gradeLevel);
    }
    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "group_gradelevel", joinColumns = {
            @JoinColumn(name = "group_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "gradelevel_id",
                    nullable = false, updatable = false) })
    public Set<GradeLevel> getGradeLevels() {
        return gradeLevels;
    }

    public void setGradeLevels(Set<GradeLevel> gradeLevels) {
        this.gradeLevels = gradeLevels;
    }

    @Override
    public String toString() {
        return String.format("Education Group [id = %d, Name = '%s', Subjects = '%s', Students = '%s']", id, groupName, subjects, students.size());
    }
}
