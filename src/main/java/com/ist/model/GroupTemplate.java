package com.ist.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by andhr on 2017-03-18.
 */
@Entity
@Table(name = "group_template")
public class GroupTemplate {

    private Long id;
    private String name;
    private int maxStudents;
    private int minStudents;
    private Set<Subject> subjects = new HashSet<>(0);
    private Set<GradeLevel> gradeLevels = new HashSet<>(0);

    public GroupTemplate() {}

    public GroupTemplate(String name, int maxStudents, int minStudents, Set<Subject> subjects, Set<GradeLevel> gradeLevels) {
        this.name = name;
        this.maxStudents = maxStudents;
        this.minStudents = minStudents;
        this.subjects = subjects;
        this.gradeLevels = gradeLevels;
    }

    public GroupTemplate(String name, int maxStudents, int minStudents) {
        this.name = name;
        this.maxStudents = maxStudents;
        this.minStudents = minStudents;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    @Column(nullable = false)
    public int getMinStudents() {
        return minStudents;
    }

    public void setMinStudents(int minStudents) {
        this.minStudents = minStudents;
    }

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "grouptemplate_subject", joinColumns = {
            @JoinColumn(name = "grouptemplate_id") },
            inverseJoinColumns = { @JoinColumn(name = "subject_id") })
    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "grouptemplate_gradelevel", joinColumns = {
            @JoinColumn(name = "grouptemplate_id") },
            inverseJoinColumns = { @JoinColumn(name = "gradelevel_id") })
    public Set<GradeLevel> getGradeLevels() {
        return gradeLevels;
    }

    public void setGradeLevels(Set<GradeLevel> gradeLevels) {
        this.gradeLevels = gradeLevels;
    }

    public void addGradeLevel(GradeLevel gradeLevel) {
        this.gradeLevels.add(gradeLevel);
    }
}
