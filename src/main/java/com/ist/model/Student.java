package com.ist.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

    private Long id;
    private String name;
    private GradeLevel gradeLevel;
    private Set<Form> forms = new HashSet<>(0);
    private Set<Subject> subjects = new HashSet<>(0);


    public Student() {}

    public Student(String name) {
        this.name = name;
    }

    public Student(String name, GradeLevel gradeLevel, Form form) {
        this.name = name;
        this.gradeLevel = gradeLevel;
        this.forms.add(form);
    }

    public Student(String name, GradeLevel gradeLevel) {
        this.name = name;
        this.gradeLevel = gradeLevel;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    public GradeLevel getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(GradeLevel gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "form_student", joinColumns = {
            @JoinColumn(name = "student_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "form_id",
                    nullable = false, updatable = false) })
    public Set<Form> getForms() {
        return forms;
    }

    public void setForms(Set<Form> forms) {
        this.forms = forms;
    }

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "student_curriculum", joinColumns = {
            @JoinColumn(name = "student_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "subject_id",
                    nullable = false, updatable = false) })
    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Student that = (Student) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(name, that.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(13, 35)
                .append(id)
                .append(name)
                .toHashCode();
    }

    @Override
    public String toString() {
        return String.format("Student [id = %d, Name = '%s', GL = '%s', Forms = '%s', Subjects = '%s']", id, name, gradeLevel, forms, subjects);
    }
}
