package com.ist.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
@Entity
@Table(name = "subject")
public class Subject implements Serializable {

    private Long id;
    private String code;
    private String name;
    private SubjectType subjectType;

    private List<Student> students = new ArrayList<>();

    public Subject() {}

    public Subject(SubjectType subjectType, String code, String name) {
        this.subjectType = subjectType;
        this.code = code;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }


    @ManyToMany(mappedBy = "subjects")
    @JsonIgnore
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return String.format("Subject [Type = %s, Code = '%s', Name = '%s']", subjectType, code, name);
    }
}
