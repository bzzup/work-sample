package com.ist.repository;

import com.ist.model.Form;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
public interface FormRepository extends JpaRepository<Form, Long> {

    List<Form> findByName(String name);
}
