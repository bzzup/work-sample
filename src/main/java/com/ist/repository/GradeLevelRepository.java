package com.ist.repository;

import com.ist.model.GradeLevel;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by andhr on 2017-03-17.
 */
public interface GradeLevelRepository extends PagingAndSortingRepository<GradeLevel, Long> {

    GradeLevel findByCode(String code);
}