package com.ist.repository;

import com.ist.model.GradeLevel;
import com.ist.model.Group;
import com.ist.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by andhr on 2017-03-19.
 */
public interface GroupRepository extends JpaRepository<Group, Long> {

    List<Group> findByGradeLevelsInAndSubjectsIn(Set<GradeLevel> gradeLevels, Set<Subject> subjects);

}
