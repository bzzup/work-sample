package com.ist.repository;

import com.ist.model.GroupTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by andhr on 2017-03-18.
 */
public interface GroupTemplateRepository extends JpaRepository<GroupTemplate, Long> {
}
