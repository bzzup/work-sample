package com.ist.repository;

import com.ist.model.Form;
import com.ist.model.GradeLevel;
import com.ist.model.Student;
import com.ist.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

    Set<Student> findByGradeLevel(GradeLevel gradeLevel);
    Set<Student> findByForms(Set<Form> forms);
    Set<Student> findBySubjects(Subject subject);
    Set<Student> findBySubjectsAndGradeLevel(Subject subject, Set<GradeLevel> gradeLevels);
}
