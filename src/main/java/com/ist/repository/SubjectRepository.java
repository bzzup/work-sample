package com.ist.repository;

import com.ist.model.Student;
import com.ist.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */
public interface SubjectRepository extends JpaRepository<Subject, Long> {

    List<Subject> findByCode(String code);
    List<Subject> findByName(String name);
    List<Subject> findByStudentsIn(List<Student> students);
}
