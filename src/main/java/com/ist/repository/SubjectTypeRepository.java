package com.ist.repository;

import com.ist.model.SubjectType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by andhr on 2017-03-18.
 */
public interface SubjectTypeRepository extends JpaRepository<SubjectType, Long> {
    SubjectType findByName(String name);
}
