package com.ist.service;

import com.ist.model.Form;

import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
public interface FormService {

    Form getForm(Long id);
    List<Form> getFormsByName(String name);
    List<Form> getAllForms();
    Form createForm(Form form);
}
