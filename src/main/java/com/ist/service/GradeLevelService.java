package com.ist.service;

import com.ist.model.GradeLevel;

import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
public interface GradeLevelService {

    GradeLevel getGradeLevel(Long id);
    GradeLevel getGradeLevelByCode(String code);
    List<GradeLevel> getAllGradeLevels();
    GradeLevel createGradeLevel(GradeLevel gradeLevel);
}
