package com.ist.service;

import com.ist.model.GradeLevel;
import com.ist.model.Group;
import com.ist.model.Subject;

import java.util.List;
import java.util.Set;

/**
 * Created by andhr on 2017-03-19.
 */
public interface GroupService {

    void createGroup(Group group);
    List<Group> getAll();
    List<Group> getByGradeLevelsAndSubjects(Set<GradeLevel> gradeLevels, Set<Subject> subjects);
}
