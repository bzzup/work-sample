package com.ist.service;

import com.ist.model.GroupTemplate;
import com.ist.model.Student;
import com.ist.model.Subject;

import java.util.List;
import java.util.Map;

/**
 * Created by andhr on 2017-03-18.
 */
public interface GroupTemplateService {

    GroupTemplate create(GroupTemplate groupTemplate);
    List<GroupTemplate> getAllTemplates();
    Map<Student, List<Subject>> generate();
}
