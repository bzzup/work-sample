package com.ist.service;

import com.ist.model.*;

import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */
public interface StudentService {

    Set<Student> getStudentsByGradeLevel(GradeLevel gradeLevel);
    Set<Student> getStudentsByGradeLevelId(Long gradeLevelId);
    Set<Student> getStudentsByForm(Form form);
    Set<Student> getStudentsByFormId(Long id);
    Set<Student> getAll();
    Student addStudentToForm(Student student, Form form);
    Student createStudent(Student student);
    Student addSubject(Student student, Subject subject);
    Set<Subject> getSubjects(Student student);
    Set<Student> getStudentsBySubject(Subject subject);
    Set<Student> getStudentsBySubjectAndGradeLevel(Subject subject, Set<GradeLevel> gradeLevels);
    void delete(Student student);
}
