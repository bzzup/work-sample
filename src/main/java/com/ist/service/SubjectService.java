package com.ist.service;

import com.ist.model.Student;
import com.ist.model.Subject;

import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
public interface SubjectService {

    Subject getSubject(Long id);
    List<Subject> getSubjectByCode(String code);
    List<Subject> getSubjectByName(String name);
    List<Subject> getAllSubjects();
    Subject createSubject(Subject subject);
    List<Subject> getByStudent(List<Student> students);

}
