package com.ist.service;

import com.ist.model.SubjectType;

/**
 * Created by andhr on 2017-03-18.
 */
public interface SubjectTypeService {

    void create(SubjectType subjectType);
    SubjectType getByName(String name);
}
