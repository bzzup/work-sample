package com.ist.service.impl;

import com.google.common.collect.Lists;
import com.ist.model.Form;
import com.ist.repository.FormRepository;
import com.ist.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */

@Service
@Transactional
public class FormServiceImpl implements FormService {

    @Autowired
    private FormRepository formRepository;

    @Override
    public Form getForm(Long id) {
        return formRepository.findOne(id);
    }

    @Override
    public List<Form> getFormsByName(String name) {
        return formRepository.findByName(name);
    }

    @Override
    public List<Form> getAllForms() {
        return Lists.newArrayList(formRepository.findAll());
    }

    @Override
    public Form createForm(Form form) {
        return formRepository.save(form);
    }
}
