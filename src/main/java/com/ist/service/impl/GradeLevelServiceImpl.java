package com.ist.service.impl;

import com.google.common.collect.Lists;
import com.ist.model.GradeLevel;
import com.ist.repository.GradeLevelRepository;
import com.ist.service.GradeLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
@Service
@Transactional
public class GradeLevelServiceImpl implements GradeLevelService {

    @Autowired
    private GradeLevelRepository gradeLevelRepository;

    @Override
    public GradeLevel getGradeLevel(Long id) {
        return gradeLevelRepository.findOne(id);
    }

    @Override
    public GradeLevel getGradeLevelByCode(String code) {
        return gradeLevelRepository.findByCode(code);
    }

    @Override
    public List<GradeLevel> getAllGradeLevels() {
        return Lists.newArrayList(gradeLevelRepository.findAll());
    }

    @Override
    public GradeLevel createGradeLevel(GradeLevel gradeLevel) {
        return gradeLevelRepository.save(gradeLevel);
    }
}
