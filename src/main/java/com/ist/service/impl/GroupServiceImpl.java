package com.ist.service.impl;

import com.ist.model.GradeLevel;
import com.ist.model.Group;
import com.ist.model.Subject;
import com.ist.repository.GroupRepository;
import com.ist.repository.StudentRepository;
import com.ist.repository.SubjectRepository;
import com.ist.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

/**
 * Created by andhr on 2017-03-19.
 */
@Service
@Transactional
public class GroupServiceImpl implements GroupService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Override
    public void createGroup(Group group) {
        groupRepository.saveAndFlush(group);
    }

    @Override
    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    @Override
    public List<Group> getByGradeLevelsAndSubjects(Set<GradeLevel> gradeLevels, Set<Subject> subjects) {
        return groupRepository.findByGradeLevelsInAndSubjectsIn(gradeLevels, subjects);
    }
}
