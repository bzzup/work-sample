package com.ist.service.impl;

import com.ist.model.*;
import com.ist.repository.GroupTemplateRepository;
import com.ist.service.GroupTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by andhr on 2017-03-18.
 */
@Service
@Transactional
public class GroupTemplatesServiceImpl implements GroupTemplateService {

    @Autowired
    private GroupTemplateRepository groupTemplateRepository;

    @Autowired
    private SubjectServiceImpl subjectService;

    @Autowired
    private StudentServiceImpl studentService;

    @Autowired
    private GroupServiceImpl groupService;

    @Override
    public GroupTemplate create(GroupTemplate groupTemplate) {
        return groupTemplateRepository.save(groupTemplate);
    }

    @Override
    public List<GroupTemplate> getAllTemplates() {
        return groupTemplateRepository.findAll();
    }

    @Override
    public Map<Student, List<Subject>> generate() {
        Map<Student, List<Subject>> unplacedStudents = new HashMap<>();
        for (Student unplacedStudent : studentService.getAll()) {
            for (Subject subject : unplacedStudent.getSubjects()) {
                unplacedStudents.computeIfAbsent(unplacedStudent, k -> new ArrayList<>());
                unplacedStudents.get(unplacedStudent).add(subject);
            }
        }
        List<Group> generatedGroups = new ArrayList<>();
        for (GroupTemplate groupTemplate : getAllTemplates()) {
            Group educationGroup = prepareGroup(groupTemplate);
            groupService.createGroup(educationGroup);
            generatedGroups.add(educationGroup);
        }

        for (Student student : unplacedStudents.keySet()) {
            List<Student> studentList = new ArrayList<>();
            studentList.add(student);
            List<Subject> subjectsIter = subjectService.getByStudent(studentList);
            for (Subject subject : subjectsIter) {
                Set<GradeLevel> gl = new HashSet<>();
                gl.add(student.getGradeLevel());
                Set<Subject> subjects = new HashSet<>();
                subjects.add(subject);
                List<Group> matchingGroups = groupService.getByGradeLevelsAndSubjects(gl, subjects);
                if (matchingGroups.size() == 1) {
                    Group group = matchingGroups.get(0);
                    group.addStudent(student);
                    unplacedStudents.get(student).remove(subject);
                    if (unplacedStudents.get(student).size() == 0) {
                        unplacedStudents.remove(student);
                    }
                    groupService.createGroup(group);
                }
            }
        }

        return unplacedStudents;
    }

    private Group prepareGroup(GroupTemplate template) {
        Group group = new Group();
        for (Subject subject : template.getSubjects()) {
            group.addSubject(subject);
        }
        for (GradeLevel gradeLevel : template.getGradeLevels()) {
            group.addGradeLevel(gradeLevel);
        }
        group.setGroupName(template.getName());
        return group;
    }
}
