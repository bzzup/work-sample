package com.ist.service.impl;

import com.ist.exception.StudentException;
import com.ist.model.*;
import com.ist.repository.FormRepository;
import com.ist.repository.GradeLevelRepository;
import com.ist.repository.StudentRepository;
import com.ist.repository.SubjectRepository;
import com.ist.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by andhr on 2017-03-17.
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private GradeLevelRepository gradeLevelRepository;

    @Override
    public Set<Student> getStudentsByGradeLevel(GradeLevel gradeLevel) {
        return studentRepository.findByGradeLevel(gradeLevel);
    }

    @Override
    public Set<Student> getStudentsByGradeLevelId(Long gradeLevelId) {
        return studentRepository.findByGradeLevel(gradeLevelRepository.findOne(gradeLevelId));
    }

    @Override
    public Set<Student> getStudentsByForm(Form form) {
        Set<Form> forms = new HashSet<>();
        forms.add(form);
        return studentRepository.findByForms(forms);
    }

    @Override
    public Set<Student> getStudentsByFormId(Long formId) {
        Set<Form> forms = new HashSet<>();
        forms.add(formRepository.findOne(formId));
        return studentRepository.findByForms(forms);
    }

    @Override
    public Set<Student> getAll() {
        return new HashSet<>(studentRepository.findAll());
    }

    @Override
    public Student addStudentToForm(Student student, Form form) {
        Student stud = studentRepository.findOne(student.getId());
        Form form1 = formRepository.findOne(form.getId());
        if (form1.getGradeLevels().contains(student.getGradeLevel())) {
            stud.getForms().add(form1);
            studentRepository.save(stud);
        } else {
            throw new StudentException("Student grade level doesn't match form grade level");
        }
        return student;
    }

    @Override
    public Student createStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student addSubject(Student student, Subject subject) {
        Student stud = studentRepository.findOne(student.getId());
        Subject subj = subjectRepository.findOne(subject.getId());
        stud.getSubjects().add(subj);
        studentRepository.save(stud);
        return stud;
    }

    @Override
    public Set<Subject> getSubjects(Student student) {
        return student.getSubjects();
    }

    @Override
    public Set<Student> getStudentsBySubject(Subject subject) {
        return studentRepository.findBySubjects(subjectRepository.findOne(subject.getId()));
    }

    @Override
    public Set<Student> getStudentsBySubjectAndGradeLevel(Subject subject, Set<GradeLevel> gradeLevels) {
        return studentRepository.findBySubjectsAndGradeLevel(subject, gradeLevels);
    }

    @Override
    public void delete(Student student) {
        studentRepository.delete(student);
    }

}
