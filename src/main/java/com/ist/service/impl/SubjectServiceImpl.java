package com.ist.service.impl;

import com.ist.model.Student;
import com.ist.model.Subject;
import com.ist.repository.SubjectRepository;
import com.ist.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by andhr on 2017-03-17.
 */
@Service
@Transactional
public class SubjectServiceImpl implements SubjectService{

    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public Subject getSubject(Long id) {
        return subjectRepository.findOne(id);
    }

    @Override
    public List<Subject> getSubjectByCode(String code) {
        return subjectRepository.findByCode(code);
    }

    @Override
    public List<Subject> getSubjectByName(String name) {
        return subjectRepository.findByName(name);
    }

    @Override
    public List<Subject> getAllSubjects() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject createSubject(Subject subject) {
        return subjectRepository.save(subject);
    }

    @Override
    public List<Subject> getByStudent(List<Student> students) {
        return subjectRepository.findByStudentsIn(students);
    }
}
