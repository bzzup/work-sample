package com.ist.service.impl;

import com.ist.model.SubjectType;
import com.ist.repository.SubjectTypeRepository;
import com.ist.service.SubjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by andhr on 2017-03-18.
 */
@Service
@Transactional
public class SubjectTypeServiceImpl implements SubjectTypeService {

    @Autowired
    private SubjectTypeRepository subjectTypeRepository;

    @Override
    public void create(SubjectType subjectType) {
        subjectTypeRepository.save(subjectType);
    }

    @Override
    public SubjectType getByName(String name) {
        return subjectTypeRepository.findByName(name);
    }
}
