package com.ist.controller;

import com.ist.repository.GroupRepository;
import com.ist.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Created by andhr on 2017-03-19.
 */
public class ControllerTest {

    protected HttpMessageConverter mappingJackson2HttpMessageConverter;

    @MockBean
    protected SubjectServiceImpl subjectService;

    @MockBean
    protected FormServiceImpl formService;

    @MockBean
    protected StudentServiceImpl studentService;

    @MockBean
    protected GroupServiceImpl groupService;

    @MockBean
    protected GroupTemplatesServiceImpl groupTemplatesService;

    @MockBean
    protected SubjectTypeServiceImpl subjectTypeService;

    @Autowired
    protected MockMvc mockMvc;

    @MockBean
    protected GradeLevelServiceImpl gradeLevelService;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);
    }

    protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
