package com.ist.controller;

import com.ist.controller.api.FormController;
import com.ist.model.Form;
import com.ist.model.GradeLevel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by andhr on 2017-03-19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FormController.class)
public class FormControllerTest extends ControllerTest {
    @Test
    public void getAllForms() throws Exception {
        Form form1 = new Form("FOrm1");
        form1.setId(1L);
        Form form2 = new Form("FOrm2");
        form2.setId(2L);
        List<Form> forms = new ArrayList<>();
        forms.add(form1);
        forms.add(form2);
        when(formService.getAllForms()).thenReturn(forms);
        this.mockMvc.perform(get("/forms/")).andExpect(status().isOk()).
                andExpect(jsonPath("$", hasSize(2))).
                andExpect(jsonPath("$[0].id", is(1))).
                andExpect(jsonPath("$[0].name", is("FOrm1")));
    }

    @Test
    public void createForm() throws Exception {
        String body = json(new Form("Form Name"));

        this.mockMvc.perform(post("/forms/form")
                .contentType(contentType)
                .content(body))
                .andExpect(status().isOk());
    }

    @Test
    public void createFormWithGL() throws Exception {
        GradeLevel gl1 = new GradeLevel("1");
        String body = json(new Form("Form Name", gl1));

        this.mockMvc.perform(post("/forms/form")
                .contentType(contentType)
                .content(body))
                .andExpect(status().isOk());
    }

    @Test
    public void createFormWithGLs() throws Exception {
        GradeLevel gl1 = new GradeLevel("1");
        GradeLevel gl2 = new GradeLevel("2");
        Set<GradeLevel> set = new HashSet<>();
        set.add(gl1);
        set.add(gl2);
        String body = json(new Form("Form Name", set));

        this.mockMvc.perform(post("/forms/form")
                .contentType(contentType)
                .content(body))
                .andExpect(status().isOk());
    }
}
