package com.ist.controller;

import com.ist.controller.api.GradeLevelController;
import com.ist.model.GradeLevel;
import com.ist.service.impl.GradeLevelServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by andhr on 2017-03-19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = GradeLevelController.class)
public class GradeLevelControllerTest extends ControllerTest {

    @Test
    public void createGradeLevel() throws Exception {
        String glJson = json(new GradeLevel("GL1"));

        this.mockMvc.perform(post("/gradelevels/gradelevel")
                .contentType(contentType)
                .content(glJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllGradeLevels() throws Exception {
        GradeLevel gl1 = new GradeLevel();
        gl1.setCode("code1");
        gl1.setId(1L);
        List<GradeLevel> list = new ArrayList<>();
        list.add(gl1);
        when(gradeLevelService.getAllGradeLevels()).thenReturn(list);
        this.mockMvc.perform(get("/gradelevels/")).andExpect(status().isOk()).
                andExpect(jsonPath("$", hasSize(1))).
                andExpect(jsonPath("$[0].id", is(1))).
                andExpect(jsonPath("$[0].code", is("code1")));
    }
}
