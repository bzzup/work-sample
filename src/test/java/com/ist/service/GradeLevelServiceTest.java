package com.ist.service;

import com.ist.model.GradeLevel;
import com.ist.repository.GradeLevelRepository;
import com.ist.service.impl.GradeLevelServiceImpl;
import com.ist.service.impl.StudentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by andhr on 2017-03-19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class GradeLevelServiceTest {

    @Configuration
    static class ClassServiceTestContextConfiguration {

        @Bean
        public GradeLevelServiceImpl gradeLevelService() {return new GradeLevelServiceImpl(); }

        @Bean
        public GradeLevelRepository gradeLevelRepository() {return mock(GradeLevelRepository.class);}

    }

    @Autowired
    GradeLevelServiceImpl gradeLevelService;

    @Autowired
    GradeLevelRepository gradeLevelRepository;

    @Test
    public void testGetGradeLevel() {
        GradeLevel gl = new GradeLevel("GL1");
        gl.setId(1L);
        when(gradeLevelService.getGradeLevel(isA(Long.class))).thenReturn(gl);
        GradeLevel expectedGL = new GradeLevel("GL1");
        expectedGL.setId(1L);
        GradeLevel actualGL = gradeLevelService.getGradeLevel(1L);
        assertEquals("Grade Levels do not match!", expectedGL, actualGL);
    }
}
